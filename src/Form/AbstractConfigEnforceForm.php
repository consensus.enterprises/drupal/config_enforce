<?php

namespace Drupal\config_enforce\Form;

use Drupal\config_enforce\ConfigEnforce;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Attached Config Enforce behaviour for config forms.
 */
abstract class AbstractConfigEnforceForm extends ConfigFormBase  {

  use StringTranslationTrait;

  /* The form array. */
  protected $form;

  /* The form state. */
  protected $formState;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return self::FORM_ID;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      ConfigEnforce::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->setSharedObjectProperties($form, $form_state);

    // Make our form float above the config form.
    $this->form['#prefix'] = "<div class='config-enforce-form'>";
    $this->form['#suffix'] = "</div>";
    $this->form['#attached']['library'][] = 'config_enforce/config-enforce';

    $this->addConfigFormInfo();

    return $this->form;
  }

  /**
   * Add submit actions and such.
   *
   * This method cannot be overridden, since parent::buildForm needs to refer
   * to ConfigFormBase::buildForm. For this to work (calling the grandparent's
   * method) it needs to be called like parent::buildBaseForm() from the child
   * class.
   */
  final protected function buildBaseForm(array $form, FormStateInterface $form_state) {
    $this->setSharedObjectProperties($form, $form_state);
    $this->form = parent::buildForm($this->form, $this->formState);
    return $this->form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->setSharedObjectProperties($form, $form_state);
  }

  /**
   * Set common properties on the object, for easier OOP.
   */
  protected function setSharedObjectProperties(array &$form, FormStateInterface &$form_state) {
    $this->form = $form;
    $this->formState = $form_state;
    // @see: AbstractConfigEnforceFormHandler::addEnforceForm()
    $this->context = $this->formState->getBuildInfo()['args'];
  }

  /**
   * Add a details section with information about the config form.
   */
  protected function addConfigFormInfo() {
    $form_id = $this->getContext('form_id');

    $this->form['config_enforce'] = [
      '#type' => 'details',
      '#tree' =>TRUE,
      '#title' => $this->t('Config form: %id', ['%id' => $form_id]),
      '#open' => TRUE,
    ];

    foreach ($this->getFormConfigs() as $config) {
      $this->form['config_enforce'][$config] = [
        '#type' => 'details',
        '#title' => $this->t('Config object: @config', ['@config' => $config]),
        '#open' => TRUE,
      ];

      $config_enforce_enabled = ConfigEnforce::isEnforced($config);
      $this->form['config_enforce'][$config]['config_enforce_enabled'] = [
        '#type' => 'item',
        '#title' => $this->t('Config enforce'),
        '#markup' => $config_enforce_enabled ? 'Enabled' : 'Disabled',
      ];

      if ($config_enforce_enabled) {
        $this->form['config_enforce'][$config]['config_enforce_module'] = [
          '#type' => 'item',
          '#title' => $this->t('Module'),
          '#markup' => ConfigEnforce::getModule($config),
        ];
        $this->form['config_enforce'][$config]['config_enforce_directory'] = [
          '#type' => 'item',
          '#title' => $this->t('Directory'),
          '#markup' => ConfigEnforce::getDirectory($config),
        ];
        $this->form['config_enforce'][$config]['config_enforce_path'] = [
          '#type' => 'item',
          '#title' => $this->t('File path'),
          '#markup' => ConfigEnforce::getPath($config),
        ];
        $this->form['config_enforce'][$config]['config_enforce_level'] = [
          '#type' => 'item',
          '#title' => $this->t('Enforcement level'),
          '#markup' => ConfigEnforce::getLevelLabel($config),
        ];
      }
    }
  }

  /**
   * Return an item passed into the form from the config form to which we are attached.
   */
  protected function getContext($key) {
    return $this->context[$key];
  }

  /**
   * Return a list of configs embedded in the current form object.
   */
  protected function getFormConfigs() {
    return $this->getContext('configs');
  }

  /**
   * Print common debug data.
   */
  protected function debug() {
    if (function_exists('dpm')) {
      dpm($this->form);
      dpm($this->formState);
      dpm($this->context);
      dpm($this->config);
    }
  }

}
