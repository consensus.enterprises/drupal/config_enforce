<?php

namespace Drupal\config_enforce\Form;

use Drupal\config_enforce\Form\AbstractConfigEnforceForm;

/**
 * Defines a form for the Config Enforce module.
 */
class ConfigEnforceForm extends AbstractConfigEnforceForm {

  const FORM_ID = 'config_enforce';

}
