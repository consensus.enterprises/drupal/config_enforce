<?php

namespace Drupal\config_enforce;

use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Enforces configuration.
 */
class ConfigEnforce {

  use StringTranslationTrait;

  const CONFIG_NAME = 'config_enforce.enforced_configs';

  const CONFIG_ENFORCE_OFF = 0;
  const CONFIG_ENFORCE_NOSUBMIT = 10;
  const CONFIG_ENFORCE_READONLY = 20;

  /**
   * Return whether a given config object is being enforced.
   */
  public static function isEnforced(string $config) {
    return !is_null(self::getConfig($config));
  }

  /**
   * Return the currently configured target module for a given config object.
   */
  public static function getModule(string $config) {
    return self::getConfig($config)['module'];
  }

  /**
   * Return the currently configured config directory for a given config object.
   */
  public static function getDirectory(string $config) {
    return self::getConfig($config)['directory'];
  }

  /**
   * Return the currently configured path of the config object.
   */
  public static function getPath(string $config) {
    return self::getConfig($config)['path'];
  }

  /**
   * Return a list of available enforcement levels.
   */
  public static function getLevels() {
    return [
      self::CONFIG_ENFORCE_OFF => t('Allow form & API updates.'),
      self::CONFIG_ENFORCE_NOSUBMIT => t('Allow only API updates.'),
      self::CONFIG_ENFORCE_READONLY => t('Read-only, no updates.'),
    ];
  }

  /**
   * Return the currently configured enforcement level for the config object.
   */
  public static function getLevel(string $config) {
    return self::getConfig($config)['level'];
  }

  /**
   * Return a human readable enforcement level for the config object.
   */
  public static function getLevelLabel(string $config) {
    return self::getLevels()[self::getLevel($config)];
  }

  /**
   * Return this module's configuration for a given config object.
   */
  public static function getConfig(string $config) {
    return \Drupal::config(self::CONFIG_NAME)->get(self::encode($config));
  }

  /**
   * Return this module's stored config.
   */
  public static function getEnforcedConfigs() {
    return \Drupal::config(self::CONFIG_NAME)->get();
  }

  /**
   * Consistently encode config names, so that they can be used as array keys when saved.
   *
   * @see decode().
   */
  public static function encode($config) {
    return str_replace('.', '__', $config);
  }

  /**
   * Consistently decode config names.
   *
   * @see encode().
   */
  public static function decode($config) {
    return str_replace('__', '.', $config);
  }

}
