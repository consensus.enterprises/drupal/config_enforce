<?php

namespace Drupal\config_enforce\FormHandler;

use Drupal\config_enforce\ConfigResolver;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Attach Config Enforce behaviour to config forms.
 */
abstract class AbstractConfigEnforceFormHandler {

  use MessengerTrait, StringTranslationTrait;

  /* The form array. */
  protected $form;

  /* The form state. */
  protected $formState;

  /* A helper object to extract config information from the form. */
  protected $configResolver;

  /**
   * A Simple constructor method.
   */
  public function __construct(&$form, FormStateInterface &$form_state) {
    $this->form =& $form;
    $this->formState =& $form_state;
    $this->configResolver = new ConfigResolver($this->formState);
  }

  /**
   * Method to call from implementations of hook_alter().
   */
  abstract public function alter();

  /**
   * Wrapper for resolver method.
   *
   * @see \Drupal\config_enforce\ConfigResolver::getFormId().
   */
  protected function getFormId() {
    return $this->configResolver->getFormId();
  }

  /**
   * Wrapper for resolver method.
   *
   * @see \Drupal\config_enforce\ConfigResolver::getConfigNames().
   */
  protected function getConfigNames() {
    return $this->configResolver->getConfigNames();
  }

  /**
   * Wrapper for resolver method.
   */
  protected function isAConfigForm() {
    return $this->configResolver->isAConfigForm();
  }

  /**
   * Embed our form in the config form.
   */
  protected function addEnforceForm($class) {
    $config_enforce_context = [
      'form_id' => $this->getFormId(),
      'configs' => $this->getConfigNames(),
    ];

    $config_factory = \Drupal::configFactory();
    $form = new $class($config_factory);
    $this->formState->addBuildInfo('args', $config_enforce_context);
    $this->form['config_enforce'] = $form->buildForm([], $this->formState);

    $this->form['#submit'][] = [$form, 'submitForm'];
  }

}
