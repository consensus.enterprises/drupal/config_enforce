<?php

namespace Drupal\config_enforce\FormHandler;

use Drupal\config_enforce\ConfigEnforce;
use Drupal\config_enforce\Form\ConfigEnforceForm;
use Drupal\config_enforce\FormHandler\AbstractConfigEnforceFormHandler;

/**
 * Attach Config Enforce behaviour to config forms.
 */
class ConfigEnforceFormHandler extends AbstractConfigEnforceFormHandler {

  const MINIMUM_ENFORCE_LEVEL = ConfigEnforce::CONFIG_ENFORCE_NOSUBMIT;

  /**
   * Method to call from implementations of hook_alter().
   */
  public function alter() {
    // Don't alter our embedded form, as that'll lead to an infinite loop.
    if ($this->getFormId() == ConfigEnforceForm::FORM_ID) return;

    // Only operate on config forms.
    if (!$this->isAConfigForm()) return;

    // Config Enforce Devel overrides this module's behaviour.
    if ($this->configEnforceDevelEnabled()) return;

    $this->addEnforceForm('Drupal\config_enforce\Form\ConfigEnforceForm');
    $this->enforceForm();
  }

  /**
   * Determine whether the Config Enforce Devel module is enabled.
   */
  protected function configEnforceDevelEnabled() {
    return \Drupal::moduleHandler()->moduleExists('config_enforce_devel');
  }

  /**
   * Apply the appropriate level of config enforcement to this form.
   */
  protected function enforceForm() {
    if (!$this->shouldEnforceForm()) return;

    $this->disableFormFields();
    $this->disableFormSubmitHandlers();
  }

  /**
   * Determine whether this form should be enforced.
   *
   * Note that the highest enforcement level, among config supported by this
   * form, will be applied.
   */
  protected function shouldEnforceForm() {
    $levels = [];
    foreach ($this->getConfigNames() as $config) {
      $levels[$config] = ConfigEnforce::getLevel($config);
    }
    return max($levels) >= self::MINIMUM_ENFORCE_LEVEL;
  }

  /**
   * Disable all form fields.
   */
  protected function disableFormFields() {
    foreach ($this->getFields($this->form) as &$field) {
      $this->disableFormField($field);
    }
  }

  /**
   * Return all fields within a given form element, by reference.
   */
  protected function getFields(&$element) {
    $fields = [];
    foreach ($element as $key => &$field) {
      if ($key[0] == '#') continue;
      $fields[$key] =& $field;
    }
    return $fields;
  }

  /**
   * Disable a given form field recursively.
   */
  protected function disableFormField(&$field) {
    foreach ($this->getFields($field) as &$sub_field) {
      $this->disableFormField($sub_field);
      $sub_field['#attributes']['disabled'] = 'disabled';
      $sub_field['#attributes']['readonly'] = 'readonly';
    }
  }

  /**
   * Disable form submit handlers.
   */
  protected function disableFormSubmitHandlers() {
    $this->form['#submit'] = [];
  }

}
