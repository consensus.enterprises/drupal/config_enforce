<?php

namespace Drupal\config_enforce;

use Drupal\Core\Form\FormStateInterface;

/**
 * Helper class to extract information from config forms.
 */
class ConfigResolver {

  /* The form object from whence to extract configuration information. */
  protected $formObject;

  /* The form state from whence to extract the form object. */
  protected $formState;

  /* The form ID. */
  protected $formId;

  /**
   * A simple constructor to set the form state and object.
   */
  public function __construct(FormStateInterface $form_state) {
    $this->formState = $form_state;
    $this->formObject = $this->determineFormObject();
    $this->formId = $this->determineFormId();
  }

  /**
   * Return an array of config names associated with the form object.
   */
  public function getConfigNames() {
    if ($this->hasSimpleConfig()) return $this->getSimpleConfigNames();
    if ($this->hasConfigEntity()) return [$this->getConfigEntityName()];
    return [];
  }

  /**
   * Return the form ID.
   */
  public function getFormId() {
    return $this->formId;
  }

  /**
   * Determine whether the form is a config form.
   */
  public function isAConfigForm() {
    // @TODO Implement a blacklist? (eg. view_preview_form)
    return (bool) count($this->getConfigNames());
  }

  /**
   * Determine whether this form contains a config entity.
   *
   * @TODO Determine whether this still needs to be public.
   */
  public function hasConfigEntity() {
    if (!method_exists($this->formObject, 'getEntity')) return FALSE;
    return method_exists($this->formObject->getEntity(), 'getConfigDependencyName');
  }

  /**
   * Return the config name from a config entity form.
   */
  protected function getConfigEntityName() {
    return $this->formObject->getEntity()->getConfigDependencyName();
  }

  /**
   * Determine whether this form contains simple config.
   */
  protected function hasSimpleConfig() {
    return method_exists($this->formObject, 'getEditableConfigNames');
  }

  /**
   * Return the config names from a simple config form.
   */
  protected function getSimpleConfigNames() {
    return $this->invokeProtectedMethod($this->formObject, 'getEditableConfigNames');
  }

  /**
   * Return the form object from the form state.
   */
  protected function determineFormObject() {
    return $this->formState->getBuildInfo()['callback_object'];
  }

  /**
   * Return the form ID from the form state.
   */
  protected function determineFormId() {
    return $this->formState->getBuildInfo()['form_id'];
  }

  /**
   * Invoke a protected method on a given object.
   */
  protected function invokeProtectedMethod($object, $method) {
    if (method_exists($object, $method)) {
      $reflection = new \ReflectionClass(get_class($object));
      $reflection_method = $reflection->getMethod($method);
      $reflection_method->setAccessible(TRUE);
      return $reflection_method->invoke($object);
    }
  }

}
